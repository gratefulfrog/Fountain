import java.util.*; 

interface SetMem{
  public boolean equals(Object p);
  public int hashCode();
}

class Point implements SetMem {
  float x,y;
  Point(float xx, float yy){
    x=xx;
    y=yy;
  }
  void prin(){
    println(x,y);
  }

  boolean equals(Object p){
    if (!(p instanceof Point)) { 
      return false; 
    }  
    // typecast o to Complex so that we can compare data members  
    Point c = (Point) p; 
    return x == c.x && y== c.y;
  }
  
   public int hashCode(){
        return (int)(x+y);
  }
}

class MSet{
  Set<SetMem> hs;
  MSet(){
    hs = new HashSet<SetMem>();
  }
  boolean add(SetMem s){
    return hs.add(s);
  }
  int size(){
    return hs.size();
  }
}


//Set<Point> s;
MSet s;
void setup(){
  size(100,100);
  //s = new HashSet<Point>();
  s = new MSet();
  Point p = new Point(0,0);
  println(s.add(p));
  Point pp = new Point(1,0);
  println(s.add(pp));
  println(s.add(p));
  Point ap = new Point(0,0);
  println(s.add(ap));
  println(s.size());
  println(p.equals(null));
  for (SetMem sm : s.hs){
    Point pip =(Point)sm;
    pip.prin();
  }
}
void draw(){}
