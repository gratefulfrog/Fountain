// note that 0° is EAST, positive is Clockwise!!
//import java.util.Map;

float R = 50;
int D = 25;    // 5 is min to get fully connected grid
int counter = 0;

HashMap <String,HPoint> map; 

class HPoint{
  float x,y,a;
  HPoint[] nextVec;
  int id;
  HPoint(float xx,float yy,float aa, HashMap <String,HPoint> m){
    x=xx;
    y=yy;
    a=aa;
    id = counter++;
    m.put(str(id),this);
    nextVec = new HPoint[]  {null,null};
  }
  void prin(int tabs){
    for (int i=0;i<tabs;i++){
      print(" ");
    }
    println(x,y,degrees(a));
    for (int i=0;i<2;i++){
      if (null!=nextVec[i]){
        nextVec[i].prin(tabs+2);
      }
    }
  }
  void addPoints(int depth,HashMap <String,HPoint> m){
    for (int i=0;i<2;i++){
      if (null==nextVec[i]){
        float nextA = a +radians(60) * (i==0 ? -1 : +1);
        float nextX = x + R*cos(nextA);
        float nextY = y + R*sin(nextA);
        HPoint foundP= getHPoint(nextX,nextY, m);
        if (foundP == null){
          nextVec[i] = new HPoint(nextX,nextY,nextA,m);
        }
        else{
          nextVec[i] = foundP;
        }
      }
    }
    if(depth>0){
      nextVec[0].addPoints(depth-1,m);
      nextVec[1].addPoints(depth-1,m);
    }
  }
  void display(HashMap<HPoint,HPoint> m){
    text(str(id),x,y);
     for (int i=0;i<2;i++){
      if (null!=nextVec[i]){
        if ((nextVec[i]!=m.get(this)) && (this!=m.get(nextVec[i]))){
          m.put(this,nextVec[i]);
          pushStyle();
          stroke((i==0 ? #00FF00 : #FF0000));
          line(x,y,nextVec[i].x,nextVec[i].y);
          popStyle();
          nextVec[i].display(m);
        }
      }
     }
  }
};

HPoint getHPoint(float x, float y, HashMap <String,HPoint> m){
  final float eps = 0.1;
  for(int i = 0;i<counter;i++){
    HPoint p = m.get(str(i));
    if (null!=p){
      if ((abs(p.x-x)<eps) && (abs(y-p.y)<eps)){
        println(p.id);
        return p;
      }
    }
  }
  println("false");
  return null;
}

HPoint hp;

void setup(){
  size(1000,1000);
  //frameRate(1);
  background(0);
  stroke(#FF0000);
  float x = width/2.0,
        y = height/2.0;
  circle(x,y,10);
  stroke(255);
  map = new HashMap <String,HPoint> (); 
  hp = new HPoint(x,y,radians(120),map);
  hp.addPoints(D,map);
  hp.display(new HashMap<HPoint,HPoint>());
}

void draw(){
}
