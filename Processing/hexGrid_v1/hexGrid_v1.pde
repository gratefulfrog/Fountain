// note that 0° is EAST, positive is Clockwise!!
//import java.util.Map;
import java.util.*; 
import java.lang.*;

final int DEPTH = 10;  // 9 takes 9 secs, 10 takes 55 secs
final float R = 20;

final color idColor = #FF0000,
            hColor  = #FFFFFF;
      
class Counter{
  int val;
  Counter(){
    val = 0;
  }
  int obtain(){
    return ++val;
  }
}

class Point{
  float x,y;
  Point(){}
  Point(float xx, float yy){
    x=xx;
    y=yy;
  }
  boolean equals(Point p){
    final float eps = 0.1;
    return ((abs(x-p.x)<eps) && (abs(y-p.y)<eps));
  }
}

class HGone {
  float r;
  Point center;
  Point[] pointVec;
  int id;
  
  HGone(float xx,float yy,float rr, Counter c, Set<HGone> hSet, int recurseDepth){
    center = new Point(xx,yy);
    r=rr;
    if(hSet.add(this)){ // only get an id if it is a new HGone
      id = c.obtain();
    }
    getPoints();
    if (0!=recurseDepth){
      recurseConstruct(c, hSet, recurseDepth-1);
    }
  }
  
  HGone(Point pp,float rr,Counter c, Set<HGone> hSet, int recurseDepth){
    center = pp;
    r = rr;
    if(hSet.add(this)){ // only get an id if it is a new HGone
      id = c.obtain();
    }
    getPoints();
    if (0!=recurseDepth){
      recurseConstruct(c, hSet, recurseDepth-1);
    }
  }

  boolean equals(Object s){
    final float eps = 0.1;
    HGone h = (HGone)s;
    return center.equals(h.center) && abs(r-h.r)<eps;
  }
  
  public int hashCode(){
        return (int)(center.x+center.y);
  }
  
  void getPoints(){
    pointVec = new Point[6];
    for (int i = 0; i<6;i++){
      float a = radians(60*i);
      pointVec[i] = new Point(center.x+r*cos(a),
                              center.y+r*sin(a));
      
    }
  }
  void recurseConstruct(Counter c, Set<HGone> hSet, int depth){
    final float dist = 2*r*cos(radians(30));
    for (int i = 30; i<360;i+=60){
      float a = radians(i);
      Point p = new Point(center.x+dist*cos(a),
                          center.y+dist*sin(a));
      new HGone(p,r,c,hSet,depth);
    }
  }
  void display(){
    pushStyle();
    stroke(hColor);
    beginShape();
    for (int i = 0; i<6;i++){
      vertex(pointVec[i].x,pointVec[i].y);
    }
    endShape(CLOSE);
    stroke(idColor);
    fill(idColor);
    text(str(id),center.x,center.y);
    popStyle();
  }
}

Set s;
Iterator iter;
void setup(){
  size(1000,1000);
  frameRate(50);
  textAlign(CENTER,CENTER);
  background(0);
  fill(0);
  stroke(#FF0000);
  Counter cc = new Counter();
  s = new HashSet<HGone>();
  float x = width/2.0,
        y = height/2.0;
 
  long timeMilli = System.currentTimeMillis();
  new HGone(new Point(x,y),R,cc,s,DEPTH);
  timeMilli = System.currentTimeMillis() - timeMilli;
  println("nb hexagons: ",s.size());
  println("computed in MilliSecs: ",timeMilli);
  stroke(255);
  iter  = s.iterator();
}
void draw(){
  if (iter.hasNext()){
    HGone h = (HGone)iter.next();
    h.display();
  }
  else{
    println("done.");
    while(true);
  }
}
