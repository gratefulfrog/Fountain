$fn=100;

ht =  5;
wt =  20;
bt = 3;
et = 0.15;

module block(mult,h){
  cube([wt,et*mult,h]);
}

module conicalHull(){
  r= bt/2.;
  hull(){
    cylinder(ht,r,et);
    translate([wt,0,0])
      cylinder(ht,r,et);
  }
}
/*
//conicalHull();
for(i=[0.97:2*et:10]){
translate([0,i*5,0])
  block(2,i);
}
*/


module rack(w,h){
  //h=w;
  l=40*w;
  cw=3*w;
  sw = 2*w;
  for(i=[0:sw:l]){
   translate([i,0,0])
    cube([w,cw,h],center=true);
  }
  translate([l/2.,0,0])
    cube([l,w,h], center=true);
}
//rack(1,1);

module crossRack(startH,endH,w,h){
  for(i=[0:endH/10.:endH*0.9]){
    //echo(i);
    ht = h*(i+1);
    translate([0,i*3*w,ht/2.])
      rack(w,ht);
  }
}

//crossRack(1,10,0.15,1);

module rings(dia){
  d=dia;
  rr = d/2.;
  linear_extrude(10){
    circle(rr);
    for (i=[0:2:10]){
      difference(){
        circle(d+(i+1)*rr);
        circle(d+i*rr);
      }
    }
  }
}
module rings_n_spokes(){
  d=0.6;
  r=d/2.;
  #rings(d);
  len=3.9-r;

  ang = 2*asin(0.5);
  e =0.01;
  eps=r*cos(ang/2.);
  lenL = 3.9-eps;
  //echo(lenL);
  //echo(ang);
  for (i=[0:1:360-ang])
  rotate([0,0,i*ang])
  translate([lenL/2.+eps-e,0,10/2.])
    #cube([lenL,r,10],center=true);
}

module embeddedPipes(n,dia,h,wall=0.3){
  r=dia/2.;
  for (i=[0:1:n-1]){
    translate([i*(dia+wall),0,0]){
      cylinder(h,r,r);
    }
  }
}
function sign(v) = (v%2)?-1:0;
//echo(sign(2));

module loads(n,dia,h,wall=0.3){
  for (i=[0:1:n-1]){
    translate([sign(i)*(dia+wall)/2.,i*(dia+wall)/2.,0])
      embeddedPipes(n-sign(i),dia,h,wall);
  }
}
module cutCyl(){
  d=1;
  w=0.3;
  n=11;
  h=10;
  eps=0.1;
  cr= (n-1)*(d+w)/2.;
  difference(){
      #cylinder(h,cr,cr);
    translate([-n*w-(n-1)*w,-n*w-(n-1)*w,-eps/2.])
      #loads(n,d,h+eps);
  }
}
/*
n=2;
d=2;
w=1;
h=20;
trX= -(n-1)*(d+w)/2.;
trY = -(n-1)*(d+w)/2. +d+w/2.;
translate([0,0,0])
loads(n,d,h,w);
*/

%linear_extrude(20)
  import("../DXF/circles_in_parallelogram.dxf", layer = "new");
%linear_extrude(30)
  import("../DXF/circles_in_parallelogram.dxf", layer = "ring");